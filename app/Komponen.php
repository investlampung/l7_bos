<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komponen extends Model
{
    protected $fillable = [
        'tahap_id','kode_komponen','komponen',
    ];

    public function tahap()
    {
        return $this->belongsTo('App\Tahap', 'tahap_id');
    }

    public function sub_komponen()
    {
        return $this->hasMany('App\SubKomponen');
    }
}
