<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKomponen extends Model
{
    protected $fillable = [
        'komponen_id','sub_komponen','kode_1','kode_2','kode_3',
    ];

    public function komponen()
    {
        return $this->belongsTo('App\Komponen', 'komponen_id');
    }

    public function nota()
    {
        return $this->hasMany('App\Nota');
    }
}
