<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $fillable = [
        'meta_key','meta_value','tahap_id'
    ];

    public function tahap()
    {
        return $this->belongsTo('App\Tahap', 'tahap_id');
    }
}
