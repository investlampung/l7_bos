<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahap extends Model
{
    protected $fillable = [
        'tahun_id','tahap',
    ];

    public function tahun()
    {
        return $this->belongsTo('App\Tahun', 'tahun_id');
    }

    public function dana()
    {
        return $this->hasMany('App\Dana');
    }

    public function saldo()
    {
        return $this->hasMany('App\Saldo');
    }

    public function barang()
    {
        return $this->hasMany('App\Barang');
    }
    public function komponen()
    {
        return $this->hasMany('App\Komponen');
    }
    public function meta()
    {
        return $this->hasMany('App\Meta');
    }
}
