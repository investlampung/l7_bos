<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $fillable = [
        'sub_komponen_id','toko_id','uraian','kode_rek','tanggal','tanggal_bku','kode_pajak','kode_pph',
    ];

    protected $dates = ['tanggal','tanggal_bku'];

    public function sub_komponen()
    {
        return $this->belongsTo('App\SubKomponen', 'sub_komponen_id');
    }
    
    public function toko()
    {
        return $this->belongsTo('App\Toko', 'toko_id');
    }

    public function nota_barang()
    {
        return $this->hasMany('App\NotaBarang');
    }

    public function bku()
    {
        return $this->hasMany('App\BKU');
    }
}
