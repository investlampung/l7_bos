<?php

namespace App\Exports;

use App\Toko;
use Maatwebsite\Excel\Concerns\FromCollection;

class TokoExport implements FromCollection
{
    public function collection()
    {
        return Toko::select('id','toko','jenis')->get();
    }
}
