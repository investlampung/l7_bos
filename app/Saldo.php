<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saldo extends Model
{
    protected $fillable = [
        'tahap_id','tanggal','saldo','ket',
    ];
    
    protected $dates = ['tanggal'];

    public function tahap()
    {
        return $this->belongsTo('App\Tahap', 'tahap_id');
    }
}
