<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BKU extends Model
{
    protected $fillable = [
        'nota_id','pajak','pph',
    ];

    public function nota()
    {
        return $this->belongsTo('App\Nota', 'nota_id');
    }
    
}
