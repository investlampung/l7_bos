<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaldoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tanggal' => 'required',
            'saldo' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'tanggal.required' => 'Tidak boleh kosong',
            'saldo.required' => 'Tidak boleh kosong'
        ];
    }
}
