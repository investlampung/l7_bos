<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tanggal' => 'required',
        ];
    }
    public function messages()
    {
        return [

            'tanggal.required' => 'Tidak boleh kosong',
        ];
    }
}
