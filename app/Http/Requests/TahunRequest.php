<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TahunRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tahun' => 'required|unique:tahuns,tahun'
        ];
    }
    public function messages()
    {
        return [

            'tahun.required' => 'Tidak boleh kosong',
            'tahun.unique' => 'Tahun sudah ada'
        ];
    }
}
