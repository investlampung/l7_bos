<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubKomponenRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'kode_1' => 'required',
            'sub_komponen' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'sub_komponen.required' => 'Tidak boleh kosong',
            'kode_1.required' => 'Tidak boleh kosong'
        ];
    }
}
