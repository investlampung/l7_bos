<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TokoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'toko' => 'required',
            'jenis' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'jenis.required' => 'Tidak boleh kosong',
            'toko.required' => 'Tidak boleh kosong'
        ];
    }
}
