<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KomponenRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'kode_komponen' => 'required',
            'komponen' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'komponen.required' => 'Tidak boleh kosong',
            'kode_komponen.required' => 'Tidak boleh kosong'
        ];
    }
}
