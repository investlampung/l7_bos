<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TahapRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tahap' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'tahap.required' => 'Tidak boleh kosong'
        ];
    }
}
