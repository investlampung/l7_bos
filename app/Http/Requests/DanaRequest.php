<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DanaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tanggal' => 'required',
            'dana' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'tanggal.required' => 'Tidak boleh kosong',
            'dana.required' => 'Tidak boleh kosong'
        ];
    }
}
