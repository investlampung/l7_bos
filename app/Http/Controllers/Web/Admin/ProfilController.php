<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Meta;
use App\Profil;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $data = Profil::all();
        $meta = Meta::where('tahap_id',$tahap_id->id)->get()->all();

        return view('admin.profil.beranda', compact('data','meta'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit(Profil $profil)
    {
        return view('admin.profil.edit', compact('profil'));
    }

    public function update(Request $request, Profil $profil)
    {
        $profil->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Profil " . $profil->profil_key . " menjadi ".$profil->profil_value;
        History::create($histori);

        $notification = array(
            'message' => 'Profil "' . $profil->profil_key . '" berhasil diubah menjadi '.$profil->profil_value,
            'alert-type' => 'success'
        );

        return redirect()->route('profil.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
