<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaldoRequest;
use App\Saldo;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class SaldoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no =1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id',$tahun_id)->where('tahap',$tahap)->get()->first();

        $saldo = Saldo::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();
        $total_saldo = Saldo::where('tahap_id', $tahap_id)->sum('saldo');
        return view('admin.saldo.beranda', compact('saldo','no','total_saldo'));
    }

    public function create()
    {
        //
    }

    public function store(SaldoRequest $request)
    {
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id',$tahun_id)->where('tahap',$tahap)->get()->first();

        $data = $request->all();
        $data['tahap_id'] = $tahap_id->id;

        Saldo::create($data);
        
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Saldo " . $request->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data Saldo "' . $request->ket . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('saldo.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(Saldo $saldo)
    {
        return view('admin.saldo.edit',compact('saldo'));
    }

    public function update(SaldoRequest $request, Saldo $saldo)
    {
        $saldo->update($request->all());
  
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Saldo " . $request->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data Saldo "' . $request->ket . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('saldo.index')->with($notification);
    }

    public function destroy(Saldo $saldo)
    {
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Saldo " . $saldo->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data Saldo "' . $saldo->ket . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $saldo->delete();

        return redirect()->route('saldo.index')->with($notification);
    }
}
