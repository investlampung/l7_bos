<?php

namespace App\Http\Controllers\Web\Admin;

use App\Dana;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\DanaRequest;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class DanaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no =1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id',$tahun_id)->where('tahap',$tahap)->get()->first();

        $dana = Dana::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();

        $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe','Masuk')->sum('dana');
        return view('admin.dana.beranda', compact('dana','no','total_dana'));
    }

    public function create()
    {
        //
    }

    public function store(DanaRequest $request)
    {
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id',$tahun_id)->where('tahap',$tahap)->get()->first();

        $data = $request->all();
        $data['tahap_id'] = $tahap_id->id;

        Dana::create($data);
        
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Dana " . $request->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data Dana "' . $request->ket . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('dana.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(Dana $dana)
    {
        return view('admin.dana.edit',compact('dana'));
    }

    public function update(DanaRequest $request, Dana $dana)
    {
        $dana->update($request->all());
  
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Dana " . $request->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data Dana "' . $request->ket . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('dana.index')->with($notification);
    }

    public function destroy(Dana $dana)
    {
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Dana " . $dana->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data Dana "' . $dana->ket . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $dana->delete();

        return redirect()->route('dana.index')->with($notification);
    }
}
