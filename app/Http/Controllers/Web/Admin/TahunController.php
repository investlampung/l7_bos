<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TahunRequest;
use App\Tahap;
use App\Tahun;
use Auth;
use Illuminate\Http\Request;

class TahunController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
    }

    public function store(TahunRequest $request)
    {
        Tahun::create($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Tahun " . $request->tahun;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tahun "' . $request->tahun . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('pengaturan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Tahun $tahun)
    {
        $tahap = Tahap::where('tahun_id', $tahun->id)->get()->all();

        if (empty($tahap)) {
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Tahun " . $tahun->tahun;
            History::create($histori);

            $notification = array(
                'message' => 'Data Tahun "' . $tahun->tahun . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $tahun->delete();

            return redirect()->route('pengaturan.index')->with($notification);
        } else {
            
            $notification = array(
                'message' => 'Data Tahun "' . $tahun->tahun . '" tidak bisa dihapus. Sedang terpakai.',
                'alert-type' => 'warning'
            );

            return redirect()->route('pengaturan.index')->with($notification);
        }
    }
}
