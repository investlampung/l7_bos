<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubKomponenRequest;
use App\Nota;
use App\SubKomponen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubKomponenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $no = 1;
        $subkomponen = SubKomponen::where('komponen_id', $id)
            ->orderBy('kode_1', 'ASC')
            ->orderBy('kode_2', 'ASC')
            ->orderBy('kode_3', 'ASC')->get()->all();
        $komponen_id = $id;

        return view('admin.subkomponen.beranda', compact('subkomponen', 'no', 'komponen_id'));
    }

    public function create()
    {
        //
    }

    public function store(SubKomponenRequest $request)
    {

        $data = $request->all();

        SubKomponen::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Sub Komponen " . $request->sub_komponen;
        History::create($histori);

        $notification = array(
            'message' => 'Data Sub Komponen "' . $request->sub_komponen . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(SubKomponen $subkomponen)
    {
        return view('admin.subkomponen.edit', compact('subkomponen'));
    }

    public function update(SubKomponenRequest $request, SubKomponen $subkomponen)
    {
        $subkomponen->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Sub Komponen " . $request->sub_komponen;
        History::create($histori);

        $notification = array(
            'message' => 'Data Sub Komponen "' . $request->sub_komponen . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect('/admin/subkomponen/index/' . $request->komponen_id)->with($notification);
    }

    public function destroy(SubKomponen $subkomponen)
    {
        $nota = Nota::where('sub_komponen_id', $subkomponen->id)->get()->first();

        if (empty($nota)) {
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Sub Komponen " . $subkomponen->sub_komponen;
            History::create($histori);

            $notification = array(
                'message' => 'Data Sub Komponen "' . $subkomponen->sub_komponen . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $subkomponen->delete();

            return redirect('/admin/subkomponen/index/' . $subkomponen->komponen_id)->with($notification);
        } else {

            $notification = array(
                'message' => 'Data Sub Komponen "' . $subkomponen->sub_komponen . '" sedang terpakai.',
                'alert-type' => 'warning'
            );

            return redirect('/admin/subkomponen/index/' . $subkomponen->komponen_id)->with($notification);
        }
    }
}
