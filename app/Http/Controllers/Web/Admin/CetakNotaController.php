<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Komponen;
use App\Nota;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CetakNotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $komponen = Komponen::where('tahap_id', $tahap_id->id)->orderBy('kode_komponen', 'ASC')->get()->all();

        return view('admin.cetak.nota.beranda', compact('komponen', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $no=1;
        $komponen = Komponen::findOrFail($id);
        return view('admin.cetak.nota.show', compact('komponen', 'no'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
