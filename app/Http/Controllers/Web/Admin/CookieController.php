<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Tahap;
use App\Tahun;
use Illuminate\Http\Request;

class CookieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if ($request->tipe == "tahun") {
            $tahun = $request->tahun;
            $id_tahun = Tahun::where('tahun', $tahun)->get()->first();
            $dtahap = Tahap::select('tahap')->where('tahun_id', $id_tahun->id)->orderBy('tahap', 'ASC')->get()->first();
            
            if (empty($dtahap)) {
                $tahap = "";

                $id_tah = $id_tahun->id;

                return redirect()->route('beranda.index')->cookie('tahun', $tahun)->cookie('tahap', $tahap)->cookie('id_tahun', $id_tah);
            } else {
                $tahap = $dtahap->tahap;

                $id_tah = $id_tahun->id;

                return redirect()->route('beranda.index')->cookie('tahun', $tahun)->cookie('tahap', $tahap)->cookie('id_tahun', $id_tah);
            }
            
            
        } else if ($request->tipe == "tahap") {
            $tahap = $request->tahap;

            return redirect()->route('beranda.index')->cookie('tahap', $tahap);
        } else {
            return redirect()->route('beranda.index');
        }
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
