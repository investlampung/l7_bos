<?php

namespace App\Http\Controllers\Web\Admin;

use App\Barang;
use App\Exports\BarangExport;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\BarangRequest;
use App\Imports\BarangImport;
use App\Nota;
use App\NotaBarang;
use App\Tahap;
use App\Toko;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Excel;

class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $barang = Barang::where('tahap_id', $tahap_id->id)->orderBy('toko_id', 'ASC')->orderBy('barang', 'ASC')->get()->all();
        
        $toko = Toko::orderBy('toko','ASC')->get()->all();
        
        return view('admin.barang.beranda', compact('barang', 'no','toko'));
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new BarangImport, $file); //IMPORT FILE 
            return redirect()->back();
        }
    }

    public function export() 
    {
        return Excel::download(new BarangExport, 'data_barang.xlsx');
    }

    public function create()
    {
        //
    }

    public function store(BarangRequest $request)
    {
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $data = $request->all();
        $data['tahap_id'] = $tahap_id->id;

        Barang::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Barang " . $request->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $request->barang . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('barang.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(Barang $barang)
    {
        return view('admin.barang.edit', compact('barang'));
    }

    public function update(BarangRequest $request, Barang $barang)
    {
        $barang->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Barang " . $request->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $request->barang . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('barang.index')->with($notification);
    }

    public function destroy(Barang $barang)
    {
        $nota = NotaBarang::where('barang_id', $barang->id)->get()->first();

        if (empty($nota)) {
            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Barang " . $barang->barang;
            History::create($histori);

            $notification = array(
                'message' => 'Data Barang "' . $barang->barang . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $barang->delete();

            return redirect()->route('barang.index')->with($notification);
        } else {

            $notification = array(
                'message' => 'Data Barang "' . $barang->barang . '" terinput di Nota.',
                'alert-type' => 'warning'
            );

            return redirect()->route('barang.index')->with($notification);
        }
    }
}
