<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\KomponenRequest;
use App\Komponen;
use App\SubKomponen;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class KomponenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $komponen = Komponen::where('tahap_id', $tahap_id->id)->orderBy('kode_komponen', 'ASC')->get()->all();

        return view('admin.komponen.beranda', compact('komponen', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(KomponenRequest $request)
    {
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $data = $request->all();
        $data['tahap_id'] = $tahap_id->id;

        Komponen::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Komponen " . $request->komponen;
        History::create($histori);

        $notification = array(
            'message' => 'Data Komponen "' . $request->komponen . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('komponen.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(Komponen $komponen)
    {
        return view('admin.komponen.edit', compact('komponen'));
    }

    public function update(KomponenRequest $request, Komponen $komponen)
    {
        $komponen->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Komponen " . $request->komponen;
        History::create($histori);

        $notification = array(
            'message' => 'Data Komponen "' . $request->komponen . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('komponen.index')->with($notification);
    }

    public function destroy(Komponen $komponen)
    {
        $subkom = SubKomponen::where('komponen_id', $komponen->id)->get()->first();

        if (empty($subkom)) {
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Komponen " . $komponen->komponen;
            History::create($histori);

            $notification = array(
                'message' => 'Data Komponen "' . $komponen->komponen . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $komponen->delete();

            return redirect()->route('komponen.index')->with($notification);
        } else {

            $notification = array(
                'message' => 'Data Komponen "' . $komponen->komponen . '" tidak bisa dihapus. Cek Sub Komponen!',
                'alert-type' => 'warning'
            );

            return redirect()->route('komponen.index')->with($notification);
        }
    }
}
