<?php

namespace App\Http\Controllers;

use App\Tahap;
use App\Tahun;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tahun = date("Y");
        $id_tahun = Tahun::where('tahun', $tahun)->get()->first();
        $dtahap = Tahap::select('tahap')->where('tahun_id',$id_tahun->id)->orderBy('tahap','ASC')->get()->first();
        $tahap = $dtahap->tahap;

        $id_tah = $id_tahun->id;

        return redirect()->route('beranda.index')->cookie('tahun',$tahun)->cookie('tahap',$tahap)->cookie('id_tahun',$id_tah);
    }
}
