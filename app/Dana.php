<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dana extends Model
{
    protected $fillable = [
        'tahap_id','tanggal','tanggal_bku','dana','ket','tipe'
    ];

    protected $dates = ['tanggal','tanggal_bku'];

    public function tahap()
    {
        return $this->belongsTo('App\Tahap', 'tahap_id');
    }
}
