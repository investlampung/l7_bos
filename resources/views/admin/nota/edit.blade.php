@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Nota
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Nota</h3>
        </div>

        <div class="box-body">
          <form action="{{ route('nota.update', $nota->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')

            <div class="col-md-12">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="toko_id" class="col-sm-4 control-label">Toko</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="toko_id">
                      <option value="{{$nota->toko_id}}">{{$nota->toko->toko}}</option>
                      @foreach($toko as $tok)
                      <option value="{{$tok->id}}">{{$tok->toko}}</option>
                      @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('toko_id') }}</small>
                  </div>
                </div>

                <div class="form-group">
                  <label for="uraian" class="col-sm-4 control-label">Uraian</label>

                  <div class="col-sm-8">
                    <input type="text" name="uraian" value="{{$nota->uraian}}" class="form-control" placeholder="Uraian (Pengganti Nama Sub Komponen)">
                    <small class="text-danger">{{ $errors->first('uraian') }}</small>
                  </div>
                </div>
                <div class="form-group">
                  <label for="kode_rek" class="col-sm-4 control-label">Kode Rekening</label>

                  <div class="col-sm-8">
                    <input type="text" name="kode_rek" value="{{$nota->kode_rek}}" class="form-control" placeholder="ex : 5-2-2-01-01">
                    <small class="text-danger">{{ $errors->first('kode_rek') }}</small>
                  </div>
                </div>

                <div class="form-group">
                  <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>

                  <div class="col-sm-8">
                    <input type="date" name="tanggal" class="form-control" value="{{ $nota->tanggal->format('Y-m-d') }}" placeholder="Tanggal">
                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tanggal_bku" class="col-sm-4 control-label">Tanggal BKU</label>

                  <div class="col-sm-8">
                    <input type="date" name="tanggal_bku" value="@if(empty($nota->tanggal_bku))@else{{ $nota->tanggal_bku->format('Y-m-d') }}@endif" class="form-control" placeholder="Tanggal BKU">
                    <small class="text-danger">{{ $errors->first('tanggal_bku') }}</small>
                  </div>
                </div>
                <div class="form-group">
                  <label for="kode_pajak" class="col-sm-4 control-label">Pajak</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="kode_pajak">
                      <option value="{{$nota->kode_pajak}}">
                        @if($nota->kode_pajak=="T")
                        Ada Pajak
                        @else
                        Tidak Ada Pajak
                        @endif
                      </option>
                      <option value="T">Ada Pajak</option>
                      <option value="F">Tidak Ada Pajak</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('kode_pajak') }}</small>
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode_pph" class="col-sm-4 control-label">PPh</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="kode_pph">
                      <option value="{{$nota->kode_pph}}">
                        @if($nota->kode_pph=="21")
                        PPh21
                        @elseif($nota->kode_pph=="22")
                        PPh22
                        @else
                        Tidak Ada PPh
                        @endif
                      </option>
                      <option value="21">PPh 21</option>
                      <option value="22">PPh 22</option>
                      <option value="-">Tidak Ada PPh</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('kode_pph') }}</small>
                  </div>
                </div>
              </div>
              <input type="hidden" name="sub_komponen_id" class="form-control" value="{{$nota->sub_komponen_id}}">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              <a href="{{url('admin/nota/index')}}/{{$nota->sub_komponen_id}}" class="btn btn-default pull-right">Kembali</a>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
@endsection