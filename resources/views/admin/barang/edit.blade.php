@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Barang
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Barang</h3>
        </div>

        <div class="box-body">
          <form action="{{ route('barang.update', $barang->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')


            <div class="form-group">
              <label for="barang" class="col-sm-2 control-label">Barang</label>

              <div class="col-sm-10">
                <input type="text" name="barang" value="{{$barang->barang}}" class="form-control" placeholder="Barang">
                <small class="text-danger">{{ $errors->first('barang') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="satuan" class="col-sm-2 control-label">Satuan</label>

              <div class="col-sm-10">
                <input type="text" name="satuan" value="{{$barang->satuan}}" class="form-control" placeholder="ex : Pcs / Box / Pak">
                <small class="text-danger">{{ $errors->first('satuan') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="harga" class="col-sm-2 control-label">Harga</label>

              <div class="col-sm-10">
                <input type="number" name="harga" value="{{$barang->harga}}" class="form-control" placeholder="ex : 10000">
                <small class="text-danger">{{ $errors->first('harga') }}</small>
              </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection