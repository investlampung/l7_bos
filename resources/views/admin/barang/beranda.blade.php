@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Barang
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a href="{{ route('exportbarang') }}" class="btn btn-primary">Download Data Barang</a><br><br>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Barang</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <div class="col-md-6">
            <form action="{{ route('barang.store') }}" class="form-horizontal" method="POST">
              @csrf

              <div class="form-group">
                <label for="toko_id" class="col-sm-2 control-label">Toko</label>

                <div class="col-sm-10">
                  <select class="form-control" name="toko_id">
                    @foreach($toko as $tok)
                    <option value="{{$tok->id}}">{{$tok->toko}}</option>
                    @endforeach
                  </select>
                  <small class="text-danger">{{ $errors->first('kode_pajak') }}</small>
                </div>
              </div>

              <div class="form-group">
                <label for="barang" class="col-sm-2 control-label">Barang</label>

                <div class="col-sm-10">
                  <input type="text" name="barang" class="form-control" placeholder="Barang">
                  <small class="text-danger">{{ $errors->first('barang') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="satuan" class="col-sm-2 control-label">Satuan</label>

                <div class="col-sm-10">
                  <input type="text" name="satuan" class="form-control" placeholder="ex : Pcs / Box / Pak">
                  <small class="text-danger">{{ $errors->first('satuan') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="harga" class="col-sm-2 control-label">Harga</label>

                <div class="col-sm-10">
                  <input type="number" name="harga" class="form-control" placeholder="ex : 10000">
                  <small class="text-danger">{{ $errors->first('harga') }}</small>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>

            </form>
          </div>
          <div class="col-md-6">
            <form action="{{ route('importbarang') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="toko" class="col-sm-4 control-label">Import Barang</label>
                <div class="col-sm-8">
                  <input type="file" name="file" class="form-control">
                  Pastikan file ber extensi .xls atau .xlsx
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Unggah</button>
            </form>
          </div>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Toko</th>
                <th>Barang</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($barang as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->toko->toko }}</td>
                <td>{{ $item->barang }}</td>
                <td>{{ $item->satuan }}</td>
                <td>{{ $item->harga }}</td>
                <td align="center">
                  <form action="{{ route('barang.destroy',$item->id) }}" method="POST">
                    <a class="btn btn-success" href="{{ route('barang.edit',$item->id) }}">Ubah</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection