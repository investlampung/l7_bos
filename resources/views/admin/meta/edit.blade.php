@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Sekolah
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Sekolah</h3>
        </div>
        <form action="{{ route('meta.update',$meta->id) }}" class="form-horizontal" method="POST">
          @csrf
          @method('PUT')
          <div class="box-body">
            <div class="form-group">
              <label for="{{$meta->meta_key}}" class="col-sm-4 control-label">{{$meta->meta_key}}</label>

              <div class="col-sm-8">
                <input type="text" name="meta_value" value="{{$meta->meta_value}}" class="form-control" placeholder="{{$meta->meta_key}}">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/profil')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection