@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Sub Komponen
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a href="{{url('admin/komponen')}}" class="btn btn-primary">Komponen</a><br><br>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Sub Komponen</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('subkomponen.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="col-md-6">
              <div class="form-group">
                <label for="kode_1" class="col-sm-4 control-label">Kode 1</label>

                <div class="col-sm-8">
                  <input type="text" name="kode_1" class="form-control" placeholder="Kode 1">
                  <small class="text-danger">{{ $errors->first('kode_1') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="kode_2" class="col-sm-4 control-label">Kode 2</label>

                <div class="col-sm-8">
                  <input type="text" name="kode_2" class="form-control" placeholder="Kode 2">
                  <small class="text-danger">{{ $errors->first('kode_2') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="kode_3" class="col-sm-4 control-label">Kode 3</label>

                <div class="col-sm-8">
                  <input type="text" name="kode_3" class="form-control" placeholder="Kode 3">
                  <small class="text-danger">{{ $errors->first('kode_3') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="komponen" class="col-sm-4 control-label">Sub Komponen</label>

                <div class="col-sm-8">
                  <input type="text" name="sub_komponen" class="form-control" placeholder="Sub Komponen">
                  <small class="text-danger">{{ $errors->first('sub_komponen') }}</small>
                </div>
              </div>
              <input type="hidden" name="komponen_id" class="form-control" value="{{$komponen_id}}">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode 1</th>
                <th>Kode 2</th>
                <th>Kode 3</th>
                <th>Sub Komponen</th>
                <th>Nota</th>
                <th>Ubah</th>
                <th>Hapus</th>
              </tr>
            </thead>
            <tbody>
              @foreach($subkomponen as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->kode_1 }}</td>
                <td>{{ $item->kode_2 }}</td>
                <td>{{ $item->kode_3 }}</td>
                <td>{{ $item->sub_komponen }}</td>
                <form action="{{ route('subkomponen.destroy',$item->id) }}" method="POST">

                  <td align="center">
                    <a class="btn btn-primary" href="{{ url('/admin/nota/index',$item->id) }}">Nota</a>
                  </td>
                  <td align="center">
                    <a class="btn btn-success" href="{{ route('subkomponen.edit',$item->id) }}">Ubah</a>
                  </td>
                  <td align="center">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </td>
                </form>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection