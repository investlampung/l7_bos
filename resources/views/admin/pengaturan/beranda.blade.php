@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Pengaturan
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tahun</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('tahun.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="box-body">
              <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Tahun</label>

                <div class="col-sm-10">
                  <input type="number" name="tahun" class="form-control" placeholder="ex : 2020">
                  <small class="text-danger">{{ $errors->first('tahun') }}</small>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Tahun</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($tahun as $tah)
              <tr>
                <td>{{ $tah->tahun }}</td>
                <td align="center">
                  <form action="{{ route('tahun.destroy',$tah->id) }}" method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tahap</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('tahap.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="box-body">
              <div class="form-group">
                <label for="tahap" class="col-sm-2 control-label">Tahap</label>

                <div class="col-sm-10">
                  <input type="text" name="tahap" class="form-control" placeholder="ex : Tahap 1 / Triwulan 1">
                  <small class="text-danger">{{ $errors->first('tahap') }}</small>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Tahap</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($tahap as $tahapp)
              <tr>
                <td>{{ $tahapp->tahap }}</td>
                <td align="center">
                  <form action="{{ route('tahap.destroy',$tahapp->id) }}" method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection