@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Dana
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Dana</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('dana.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="col-md-6">
              <div class="form-group">
                <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>

                <div class="col-sm-10">
                  <input type="date" name="tanggal" class="form-control" placeholder="Tanggal">
                  <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tanggal_bku" class="col-sm-2 control-label">Tanggal BKU</label>

                <div class="col-sm-10">
                  <input type="date" name="tanggal_bku" class="form-control" placeholder="Tanggal BKU">
                  <small class="text-danger">{{ $errors->first('tanggal_bku') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tipe" class="col-sm-2 control-label">Tipe</label>
                <div class="col-sm-10">
                  <select class="form-control" name="tipe">
                    <option value="Tarik">Tarik</option>
                    <option value="Masuk">Masuk</option>
                  </select>
                  <small class="text-danger">{{ $errors->first('tipe') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Dana</label>

                <div class="col-sm-10">
                  <input type="number" name="dana" class="form-control" placeholder="Dana">
                  <small class="text-danger">{{ $errors->first('dana') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="ket" class="col-sm-2 control-label">Keterangan</label>

                <div class="col-sm-10">
                  <input type="text" name="ket" class="form-control" placeholder="ex : Penarikan Dana 1">
                  <small class="text-danger">{{ $errors->first('ket') }}</small>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <h3><b>Total Dana : {{ number_format($total_dana, 0, ".", ".") }}</b></h3>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Tanggal BKU</th>
                <th>Tipe</th>
                <th>Dana</th>
                <th>Keterangan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($dana as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->tanggal->format('d-m-Y') }}</td>
                <td>{{ $item->tanggal_bku->format('d-m-Y') }}</td>
                <td>{{ $item->tipe }}</td>
                <td>{{ number_format($item->dana, 0, ".", ".") }}</td>
                <td>{{ $item->ket }}</td>
                <td align="center">
                  <form action="{{ route('dana.destroy',$item->id) }}" method="POST">
                    <a class="btn btn-success" href="{{ route('dana.edit',$item->id) }}">Ubah</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection