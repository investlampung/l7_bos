@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section><br><br>

    <div class="row">
        <div class="col-md-12" align="center">
            <img src="{{ asset ('itlabil/image/default/logo.png') }}" alt="" width="200px">
            <h2>
                APLIKASI BANTUAN OPERASIONAL SEKOLAH<br>
                SMK PELITA PESAWARAN<br>
                LAMPUNG
            </h2>
        </div>
    </div>
</div>
@endsection