@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Saldo
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Saldo</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('saldo.update', $saldo->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')

            <div class="col-md-12">
              <div class="form-group">
                <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>

                <div class="col-sm-10">
                  <input type="date" name="tanggal" class="form-control" value="{{ $saldo->tanggal->format('Y-m-d') }}">
                  <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Saldo</label>

                <div class="col-sm-10">
                  <input type="number" name="saldo" value="{{ $saldo->saldo }}" class="form-control" placeholder="Saldo">
                  <small class="text-danger">{{ $errors->first('saldo') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="ket" class="col-sm-2 control-label">Keterangan</label>

                <div class="col-sm-10">
                  <input type="text" name="ket" value="{{ $saldo->ket }}" class="form-control" placeholder="ex : Penarikan saldo 1">
                  <small class="text-danger">{{ $errors->first('ket') }}</small>
                </div>
              </div>
              <a href="{{url('admin/saldo')}}" class="btn btn-default">Kembali</a>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection