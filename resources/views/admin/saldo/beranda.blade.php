@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Saldo
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Saldo</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('saldo.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="col-md-6">
              <div class="form-group">
                <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>

                <div class="col-sm-10">
                  <input type="date" name="tanggal" class="form-control" placeholder="Tanggal">
                  <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="saldo" class="col-sm-2 control-label">Saldo</label>

                <div class="col-sm-10">
                  <input type="number" name="saldo" class="form-control" placeholder="Saldo">
                  <small class="text-danger">{{ $errors->first('saldo') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="ket" class="col-sm-2 control-label">Keterangan</label>

                <div class="col-sm-10">
                  <input type="text" name="ket" class="form-control" placeholder="ex : Saldo Bulan Januari">
                  <small class="text-danger">{{ $errors->first('ket') }}</small>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <h3><b>Total Saldo : {{ number_format($total_saldo, 0, ".", ".") }}</b></h3>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Saldo</th>
                <th>Keterangan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($saldo as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->tanggal->format('d-m-Y') }}</td>
                <td>{{ number_format($item->saldo, 0, ".", ".") }}</td>
                <td>{{ $item->ket }}</td>
                <td align="center">
                  <form action="{{ route('saldo.destroy',$item->id) }}" method="POST">
                    <a class="btn btn-success" href="{{ route('saldo.edit',$item->id) }}">Ubah</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection