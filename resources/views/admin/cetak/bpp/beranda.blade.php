@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Buku Pembantu Pajak
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-primary" target="_blank" href="{{ route('bpp.show',1) }}"><i class="fa fa-print"></i> Cetak BPP</a>
      <br><br>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Buku Pembantu Pajak Periode {{Cookie::get('tahap')}} Th. {{Cookie::get('tahun')}}</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2" width="20px" align="center">No</th>
                <th rowspan="2" align="center" width="100px">Tanggal</th>
                <th rowspan="2" align="center" width="300px">Uraian</th>
                <th rowspan="2" align="center">Nilai</th>
                <th rowspan="2" align="center">DPP</th>
                <th colspan="5" align="center">Pemungutan / Pemotongan Pajak</th>
                <th rowspan="2" align="center">Penyetoran PPn, PPh</th>
                <th rowspan="2" align="center">No Bukti</th>
                <th rowspan="2" align="center">Saldo</th>
                <th rowspan="2" align="center">Keterangan</th>
              </tr>
              <tr>
                <th>PPn</th>
                <th>PPh.21</th>
                <th>PPh.22</th>
                <th>PPn.23</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <tr style="background-color:#ff9898; font-weight:bold;">
                <td>A</td>
                <td></td>
                <td>PENERIMAAN PAJAK</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @php
              $total = 0;
              $total_ppn=0;
              $total_pph21=0;
              $total_pph22=0;
              $totalll=0;
              @endphp
              @foreach($komponen as $item)
              @foreach($item->sub_komponen as $subkom)
              @foreach($subkom->nota->sortBy('tanggal') as $nota)
              @php
              $total_pajak = 0;
              $jum_harga = 0;
              @endphp

              <!-- NOTA BARANG -->
              @foreach($nota->nota_barang as $hem)
              @php
              $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
              @endphp
              @endforeach

              <!-- IF KODE PAJAK -->
              @if($nota->kode_pajak=="T")
              <!-- IF KODE PPH 22-->
              @if($nota->kode_pph=="22")
              @if($jum_harga>=1000000)
              @php
              $dasar_pajak = (100/110)*$jum_harga;
              $ppn = (10/100)*$dasar_pajak;
              $r_ppn = round($ppn, -2);
              $pph22 = (1.5/100)*$dasar_pajak;
              $r_pph22 = round($pph22, -2);
              $total_pajak = $r_ppn+$r_pph22;
              $total = $total+$total_pajak;
              $total_ppn = $total_ppn+$r_ppn;
              $total_pph22 = $total_pph22+$r_pph22;
              $totalll = $totalll+$total_pajak;
              @endphp
              <tr>
                <td>{{$no++}}</td>
                <td>{{ $nota->tanggal->format('d-m-Y') }}</td>
                <td>
                  @if($nota->uraian=="")
                  {{$nota->sub_komponen->sub_komponen}}
                  @else
                  {{$nota->uraian}}
                  @endif
                </td>
                <td align="right">{{ number_format($jum_harga, 0, ".", ".")}}</td>
                <td align="right">{{ number_format($dasar_pajak, 0, ".", ".")}}</td>
                <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
                <td></td>
                <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
                <td></td>
                <td align="right">{{ number_format($total_pajak, 0, ".", ".")}}</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
                <td></td>
              </tr>
              @endif
              <!-- END KODE PPH 22 -->
              <!-- IF KODE PPH 21 -->
              @elseif($nota->kode_pph=="21")
              @if($jum_harga>=1000000)
              @php
              $dasar_pajak = (100/110)*$jum_harga;
              $pph21 = (5/100)*$dasar_pajak;
              $r_pph21 = round($pph21, -2);
              $total_pajak = $r_pph21;
              $total = $total+$total_pajak;
              $total_pph21 = $total_pph21+$r_pph21;
              $totalll = $totalll+$total_pajak;
              @endphp
              <tr>
                <td>{{$no++}}</td>
                <td>{{ $nota->tanggal_bku->format('d-m-Y') }}</td>
                <td>
                  @if($nota->uraian=="")
                  {{$nota->sub_komponen->sub_komponen}}
                  @else
                  {{$nota->uraian}}
                  @endif
                </td>
                <td align="right">{{ number_format($jum_harga, 0, ".", ".")}}</td>
                <td align="right">{{ number_format($dasar_pajak, 0, ".", ".")}}</td>
                <td></td>
                <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($total_pajak, 0, ".", ".")}}</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
                <td></td>
              </tr>
              @endif
              @else
              @endif
              <!-- END IF KODE PPH21 -->
              @else

              @endif
              <!-- END IF KODE PAJAK -->

              @endforeach
              @endforeach
              @endforeach
              <tr height="20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr style="background-color:#ff9898; font-weight:bold;">
                <td>B</td>
                <td></td>
                <td>PENYETORAN PAJAK</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($total_ppn, 0, ".", ".")}}</td>
                <td align="right">{{ number_format($total_pph21, 0, ".", ".")}}</td>
                <td align="right">{{ number_format($total_pph22, 0, ".", ".")}}</td>
                <td></td>
                <td align="right">{{ number_format($totalll, 0, ".", ".")}}</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection