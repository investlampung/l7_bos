<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>Buku Pembantu Pajak</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">

  <style>
    @media print {
      @page {
        size: auto !important
      }
    }

    .samping {
      padding: 15px 15px;
    }

    body {
      font-family: 'Source Sans Pro', sans-serif;
    }

    #tabless {
      border: 0px solid black;
    }

    .tablee,
    td,
    th {
      border: 1px solid black;
    }

    .tablee {
      border-collapse: collapse;
      width: 100%;
      margin-bottom: 10px;
    }

    th {
      padding: 5px 5px;
      height: 20px;
      text-align: center;
      font-weight: bold;
    }

    td {
      padding-left: 5px;
      padding-right: 5px;
    }
  </style>
</head>

<body>
  <!-- onload="window.print()" -->
  <div class="samping">
    <center>
      <h4 class="box-title">BUKU PEMBANTU PAJAK</h4>
    </center>
    <br>
    <div class="row">
      <div class="col-2">Nama Sekolah</div>
      <div class="col-3">: {{$nama->profil_value}}</div>
      <div class="col"></div>
      <div class="col-2">Besar Bantuan</div>
      <div class="col-2">: Rp. {{ number_format($total_dana, 0, ".", ".")}}</div>
    </div>
    <div class="row">
      <div class="col-2">Kabupaten</div>
      <div class="col-3">: {{$kab->profil_value}}</div>
      <div class="col"></div>
      <div class="col-2">Tanggal Terima</div>
      <div class="col-2">: {{ tanggal_local($tgl_terima->tanggal) }}</div>
    </div>
    <div class="row">
      <div class="col-2">Provinsi</div>
      <div class="col-3">: {{$prov->profil_value}}</div>
      <div class="col"></div>
      <div class="col-2">Tanggal Terakhir Transaksi</div>
      <div class="col-2">: {{ tanggal_local($tgl_terakhir->tanggal) }}</div>
    </div>
    <div class="row">
      <div class="col-2">Jenis Kegiatan</div>
      <div class="col-3">: {{$jenis->meta_value}}</div>
      <div class="col"></div>
      <div class="col-2">Tanggal No. SP Revisi RAB</div>
      <div class="col-2">: {{$revisi_rab->meta_value}}</div>
    </div>
    <div class="row">
      <div class="col-2">Pemberi Bantuan</div>
      <div class="col-4">: {{$pemberi->meta_value}}</div>
      <div class="col"></div>
      <div class="col-2">Tahun Anggaran</div>
      <div class="col-2">: {{Cookie::get('tahun')}}</div>
    </div>
    <div class="row">
      <div class="col-2">Periode</div>
      <div class="col-3">: {{Cookie::get('tahap')}}</div>
      <div class="col">PERHITUNGANNYA</div>
      <div class="col-2">Bulan</div>
      <div class="col-2">: {{$bulan->meta_value}} {{Cookie::get('tahun')}}</div>
    </div>
    <br>
    <table class="tablee">
      <thead>
        <tr>
          <th rowspan="2" width="20px" align="center">No</th>
          <th rowspan="2" align="center" width="100px">Tanggal</th>
          <th rowspan="2" align="center" width="500px">Uraian</th>
          <th rowspan="2" align="center">Nilai</th>
          <th rowspan="2" align="center">DPP</th>
          <th colspan="5" align="center">Pemungutan / Pemotongan Pajak</th>
          <th rowspan="2" align="center">Penyetoran PPn, PPh</th>
          <th rowspan="2" align="center">No Bukti</th>
          <th rowspan="2" align="center">Saldo</th>
          <th rowspan="2" align="center">Keterangan</th>
        </tr>
        <tr>
          <th>PPn</th>
          <th>PPh.21</th>
          <th>PPh.22</th>
          <th>PPn.23</th>
          <th>Jumlah</th>
        </tr>
      </thead>
      <tbody>
        <tr style="background-color:#ff9898; font-weight:bold;">
          <td>A</td>
          <td></td>
          <td>PENERIMAAN PAJAK</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        @php
        $total = 0;
        $total_ppn=0;
        $total_pph21=0;
        $total_pph22=0;
        $totalll=0;
        @endphp
        @foreach($komponen as $item)
        @foreach($item->sub_komponen as $subkom)
        @foreach($subkom->nota->sortBy('tanggal') as $nota)
        @php
        $total_pajak = 0;
        $jum_harga = 0;
        @endphp

        <!-- NOTA BARANG -->
        @foreach($nota->nota_barang as $hem)
        @php
        $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
        @endphp
        @endforeach

        <!-- IF KODE PAJAK -->
        @if($nota->kode_pajak=="T")
        <!-- IF KODE PPH 22-->
        @if($nota->kode_pph=="22")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $ppn = (10/100)*$dasar_pajak;
        $r_ppn = round($ppn, -2);
        $pph22 = (1.5/100)*$dasar_pajak;
        $r_pph22 = round($pph22, -2);
        $total_pajak = $r_ppn+$r_pph22;
        $total = $total+$total_pajak;
        $total_ppn = $total_ppn+$r_ppn;
        $total_pph22 = $total_pph22+$r_pph22;
        $totalll = $totalll+$total_pajak;
        @endphp
        <tr>
          <td>{{$no++}}</td>
          <td>{{ $nota->tanggal->format('d-m-Y') }}</td>
          <td>
            @if($nota->uraian=="")
            {{$nota->sub_komponen->sub_komponen}}
            @else
            {{$nota->uraian}}
            @endif
          </td>
          <td align="right">{{ number_format($jum_harga, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($dasar_pajak, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
          <td></td>
          <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
          <td></td>
          <td align="right">{{ number_format($total_pajak, 0, ".", ".")}}</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
          <td></td>
        </tr>
        @endif
        <!-- END KODE PPH 22 -->
        <!-- IF KODE PPH 21 -->
        @elseif($nota->kode_pph=="21")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $pph21 = (5/100)*$dasar_pajak;
        $r_pph21 = round($pph21, -2);
        $total_pajak = $r_pph21;
        $total = $total+$total_pajak;
        $total_pph21 = $total_pph21+$r_pph21;
        $totalll = $totalll+$total_pajak;
        @endphp
        <tr>
          <td>{{$no++}}</td>
          <td>{{ $nota->tanggal_bku->format('d-m-Y') }}</td>
          <td>
            @if($nota->uraian=="")
            {{$nota->sub_komponen->sub_komponen}}
            @else
            {{$nota->uraian}}
            @endif
          </td>
          <td align="right">{{ number_format($jum_harga, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($dasar_pajak, 0, ".", ".")}}</td>
          <td></td>
          <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($total_pajak, 0, ".", ".")}}</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
          <td></td>
        </tr>
        @endif
        @else
        @endif
        <!-- END IF KODE PPH21 -->
        @else

        @endif
        <!-- END IF KODE PAJAK -->

        @endforeach
        @endforeach
        @endforeach
        <tr height="20px">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr style="background-color:#ff9898; font-weight:bold;">
          <td>B</td>
          <td></td>
          <td>PENYETORAN PAJAK</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($total_ppn, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($total_pph21, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($total_pph22, 0, ".", ".")}}</td>
          <td></td>
          <td align="right">{{ number_format($totalll, 0, ".", ".")}}</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
          <td></td>
        </tr>
        <tr height="20px">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <div class="row">
      <div class="col"></div>
      <div class="col-3">
        @foreach($profil as $prof)
        @if($prof->profil_key==="Desa")
        {{$prof->profil_value}},
        @endif
        @endforeach
        ......................... {{Cookie::get('tahun')}}
      </div>
    </div>
  </div>

</body>

</html>