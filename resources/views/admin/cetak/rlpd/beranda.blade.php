@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Rekapitulasi Laporan Penggunaan Dana
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-primary" target="_blank" href="{{ route('rlpd.show',1) }}"><i class="fa fa-print"></i> Cetak RLPD</a>
      <br><br>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Rekapitulasi Laporan Penggunaan Dana - {{Cookie::get('tahap')}} Tahun {{Cookie::get('tahun')}}</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2" width="20px" align="center">No</th>
                <th rowspan="2" align="center" width="100px">Tanggal</th>
                <th colspan="2" align="center">Transaksi</th>
                <th rowspan="2" align="center">Kode Rekening</th>
                <th rowspan="2" align="center" width="300px">Jenis Kegiatan</th>
                <th rowspan="2" align="center">Nama Barang</th>
                <th rowspan="2" align="center">Volume</th>
                <th rowspan="2" align="center">Satuan</th>
                <th colspan="2" align="center">Harga</th>
              </tr>
              <tr>
                <th>Bukti</th>
                <th width="100px">Tanggal</th>
                <th>Satuan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              @php
              $totalll = 0;
              @endphp

              @foreach($komponen as $item)
              @foreach($item->sub_komponen as $subkom)
              @foreach($subkom->nota->sortBy('tanggal') as $nota)
              @php
              $total_pajak = 0;
              $jum_harga = 0;
              $total = 0;
              $j=0;
              @endphp

              <!-- NOTA BARANG -->
              @foreach($nota->nota_barang as $hem)
              @php
              $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
              @endphp

              @if($j==0)
              <tr>
                <td align="center">{{$no}}</td>
                <td align="center">
                  @if(empty($item->tanggal_bku))
                  @else
                  Tgl BKU : {{ $item->tanggal_bku->format('d-m-Y') }}
                  @endif
                </td>
                <td align="center">{{$no}}</td>
                <td align="center">{{ $hem->nota->tanggal->format('d-m-Y') }}</td>
                <td align="center">{{$hem->nota->kode_rek}}</td>
                <td>
                  @if($nota->uraian=="")
                  Pelunasan {{$nota->sub_komponen->sub_komponen}}
                  @else
                  Pelunasan {{$nota->uraian}}
                  @endif
                </td>
                <td>{{$hem->barang->barang}}</td>
                <td align="center">{{$hem->qty}}</td>
                <td align="center">{{$hem->barang->satuan}}</td>
                <td align="right">{{$hem->barang->harga}}</td>
                <td align="right">{{ number_format($hem->barang->harga*$hem->qty, 0, ".", ".")}}</td>
              </tr>
              @else
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$hem->barang->barang}}</td>
                <td align="center">{{$hem->qty}}</td>
                <td align="center">{{$hem->barang->satuan}}</td>
                <td align="right">{{$hem->barang->harga}}</td>
                <td align="right">{{ number_format($hem->barang->harga*$hem->qty, 0, ".", ".")}}</td>
              </tr>
              @endif
              @php $j++ @endphp
              @endforeach

              <tr height="20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($jum_harga, 0, ".", ".")}}</td>
              </tr>

              @php $no++ @endphp
              <!-- IF KODE PAJAK -->
              @if($nota->kode_pajak=="T")
              <!-- IF KODE PPH 22-->
              @if($nota->kode_pph=="22")
              @if($jum_harga>=1000000)
              @php
              $dasar_pajak = (100/110)*$jum_harga;
              $ppn = (10/100)*$dasar_pajak;
              $r_ppn = round($ppn, -2);
              $pph22 = (1.5/100)*$dasar_pajak;
              $r_pph22 = round($pph22, -2);
              $total_pajak = $r_ppn+$r_pph22;
              $total = $jum_harga+$total_pajak;
              $totalll = $totalll+$total;
              @endphp
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">Pembayaran PPn</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">Pembayaran PPh.22</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
                <td></td>
              </tr>
              @endif
              <!-- END KODE PPH 22 -->
              <!-- IF KODE PPH 21 -->
              @elseif($nota->kode_pph=="21")
              @if($jum_harga>=1000000)
              @php
              $dasar_pajak = (100/110)*$jum_harga;
              $pph21 = (5/100)*$dasar_pajak;
              $r_pph21 = round($pph21, -2);
              $total_pajak = $r_pph21;
              $total = $jum_harga+$total_pajak;
              $totalll = $totalll+$total;
              @endphp
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">Pembayaran PPh.21</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
                <td></td>
              </tr>
              @endif
              @else
              @endif
              <!-- END IF KODE PPH21 -->
              @else
              @php
              $total = $jum_harga;
              $totalll = $totalll+$total;
              @endphp
              @endif
              <!-- END IF KODE PAJAK -->

              <tr style="background-color:#ff9898; font-weight:bold;">
                <td align="center" colspan="10">Jumlah</td>
                <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
              </tr>
              @endforeach
              @endforeach
              @endforeach
              <tr height="20px">
                <td align="center" colspan="10"></td>
                <td></td>
              </tr>
              <tr style="background-color:#71bd68; font-weight:bold;">
                <td align="center" colspan="10">Total Pengeluaran</td>
                <td align="right">{{ number_format($totalll, 0, ".", ".")}}</td>
              </tr>
              <tr style="background-color:#ead35e; font-weight:bold;">
                <td align="center" colspan="10">Total Penerimaan</td>
                <td align="right">{{ number_format($total_dana, 0, ".", ".")}}</td>
              </tr>
              <tr style="background-color:#658dd8; font-weight:bold;">
                <td align="center" colspan="10">Saldo Kas</td>
                <td align="right">{{ number_format($saldo, 0, ".", ".")}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection