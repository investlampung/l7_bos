@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Buku Kas Umum
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-primary" target="_blank" href="{{ route('bku.show',1) }}"><i class="fa fa-print"></i> Cetak BKU</a>
      <br><br>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Buku Kas Umum Periode {{Cookie::get('tahap')}} Th. {{Cookie::get('tahun')}}</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2" width="20px" align="center">No</th>
                <th rowspan="2" align="center">Tanggal</th>
                <th colspan="2" align="center">Transaksi</th>
                <th rowspan="2" align="center">Kode Rekening</th>
                <th rowspan="2" align="center">Uraian</th>
                <th colspan="2" align="center">Mutasi</th>
                <th rowspan="2" align="center">Saldo</th>
                <th rowspan="2" align="center">Ket</th>
              </tr>
              <tr>
                <th align="center">Bukti</th>
                <th align="center">Tanggal</th>
                <th align="center">Debet</th>
                <th align="center">Kridit</th>
              </tr>
            </thead>
            <tbody>
              @php
              $total_saldo=0;
              $total_debet=0;
              @endphp
              <!-- SALDO -->
              @foreach($saldo as $sald)
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$sald->ket}}</td>
                <td></td>
                <td></td>
                <td align="right">{{ number_format($sald->saldo, 0, ".", ".")}}</td>
                @php
                $total_saldo = $total_saldo+$sald->saldo;
                @endphp
                <td></td>
              </tr>
              @endforeach
              <!-- END SALDO -->
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <!-- DANA -->

              @php
              $tanggal_urut;
              $tanggal_temp;
              $no_kode="";
              @endphp

              @foreach($komponen as $item)
              @foreach($item->sub_komponen as $subkom)

              @foreach($subkom->nota->sortBy('tanggal') as $nota)
              @php
              $tanggal_urut = $nota->tanggal;
              @endphp

              @php $total = 0; @endphp

              @foreach($dana as $data)

              @if(empty($no_kode))
              @php
              $tanggal_temp = $data->tanggal_bku;
              $no_kode++;
              @endphp
              @endif

              @if($tanggal_temp < $data->tanggal_bku)
                @php
                $tanggal_temp=$data->tanggal_bku;
                @endphp
                @endif
                @if($tanggal_urut >= $data->tanggal_bku and $data->tanggal_bku == $tanggal_temp)
                @if($data->tipe==="Tarik")
                <tr style="background-color:#ff9898">
                  <td></td>
                  <td align="center">{{ $data->tanggal->format('d-m-Y') }}</td>
                  <td></td>
                  <td>{{ $data->tanggal_bku->format('d-m-Y') }}</td>
                  <td></td>
                  <td>{{$data->ket}}</td>
                  <td align="right">{{ number_format($data->dana, 0, ".", ".")}}</td>
                  <td></td>
                  <td align="right">
                    @php
                    $total_saldo = $total_saldo+$data->dana;
                    @endphp
                    {{ number_format($total_saldo, 0, ".", ".")}}
                  </td>
                  <td></td>
                </tr>
                @endif
                @endif
                @endforeach
                <tr>
                  @php $m = $no; @endphp
                  <td align="center">{{ $no++ }}</td>
                  <td align="center">
                    @if(empty($nota->tanggal_bku))
                    @else
                    {{ $nota->tanggal_bku->format('d-m-Y') }}
                    @endif
                  </td>
                  <td align="center">{{$m}}</td>
                  <td>{{ $nota->tanggal->format('d-m-Y') }}</td>
                  <td align="center">{{$nota->kode_rek}}</td>
                  <td>
                    @if($nota->uraian=="")
                    Pelunasan {{$nota->sub_komponen->sub_komponen}}
                    @else
                    Pelunasan {{$nota->uraian}}
                    @endif
                  </td>
                  <!-- <td></td> -->
                  <td>{{$nota->toko->toko}}</td>
                  <td align="right">
                    @php $jum_harga = 0; @endphp
                    <!-- NOTA BARANG -->
                    @foreach($nota->nota_barang as $hem)
                    @php
                    $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
                    @endphp
                    @endforeach
                    <!-- END NOTA BARANG -->
                    {{ number_format($jum_harga, 0, ".", ".")}}
                  </td>
                  <td align="right">
                    @php
                    $total_saldo = $total_saldo-$jum_harga;
                    @endphp
                    {{ number_format($total_saldo, 0, ".", ".")}}
                  </td>
                  <td></td>
                </tr>
                @php
                $total_debet=$total_debet+$jum_harga;
                @endphp
                <!-- IF KODE PAJAK -->
                @if($nota->kode_pajak=="T")
                <!-- IF KODE PPH 22-->
                @if($nota->kode_pph=="22")
                @if($jum_harga>=1000000)
                @php
                $dasar_pajak = (100/110)*$jum_harga;
                $ppn = (10/100)*$dasar_pajak;
                $r_ppn = round($ppn, -2);
                $pph22 = (1.5/100)*$dasar_pajak;
                $r_pph22 = round($pph22, -2);
                $total_debet=$total_debet+$r_pph22+$r_ppn;
                @endphp
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="right">PPn</td>
                  <td></td>
                  <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
                  <td align="right">
                    @php
                    $total_saldo = $total_saldo-$r_ppn;
                    @endphp
                    {{ number_format($total_saldo, 0, ".", ".")}}
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="right">PPh.22</td>
                  <td></td>
                  <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
                  <td align="right">
                    @php
                    $total_saldo = $total_saldo-$r_pph22;
                    @endphp
                    {{ number_format($total_saldo, 0, ".", ".")}}
                  </td>
                  <td></td>
                </tr>
                @endif
                <!-- END KODE PPH 22 -->
                <!-- IF KODE PPH 21 -->
                @elseif($nota->kode_pph=="21")
                @if($jum_harga>=1000000)
                @php
                $dasar_pajak = (100/110)*$jum_harga;
                $pph21 = (5/100)*$dasar_pajak;
                $r_pph21 = round($pph21, -2);
                $total_debet=$total_debet+$r_pph21;
                @endphp
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="right">PPh.21</td>
                  <td></td>
                  <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
                  <td align="right">
                    @php
                    $total_saldo = $total_saldo-$r_pph21;
                    @endphp
                    {{ number_format($total_saldo, 0, ".", ".")}}
                  </td>
                  <td></td>
                </tr>
                @endif
                @else
                @endif
                <!-- END IF KODE PPH21 -->
                @else

                @endif
                <!-- END IF KODE PAJAK -->
                @php
                $tanggal_temp=$nota->tanggal;
                @endphp
                @endforeach
                @endforeach
                @endforeach
                <tr height="20px">
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr style="background-color:#ff9898; font-weight:bold;">
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="center">TOTAL PENGELUARAN</td>
                  <td align="right">{{ number_format($total_dana, 0, ".", ".")}}</td>
                  <td align="right">{{ number_format($total_debet, 0, ".", ".")}}</td>
                  <td align="right">{{ number_format($total_saldo, 0, ".", ".")}}</td>
                  <td></td>
                </tr>
                <tr height="20px">
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection