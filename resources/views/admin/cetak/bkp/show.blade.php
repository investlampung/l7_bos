<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>BUKTI KAS PENGELUARAN</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">

  <style>
    @media print {
      @page {
        size: auto !important
      }
    }

    .samping {
      padding: 15px 15px;
    }

    body {
      font-family: 'Source Sans Pro', sans-serif;
    }

    #tabless {
      border: 0px solid black;
    }

    .tablee,
    td,
    th {
      border: 1px solid black;
    }

    .tablee {
      border-collapse: collapse;
      width: 100%;
      margin-bottom: 10px;
    }

    th {
      padding: 5px 5px;
      height: 20px;
      text-align: center;
      font-weight: bold;
    }

    td {
      padding-left: 5px;
      padding-right: 5px;
    }
  </style>
</head>

<body>
  <!-- onload="window.print()" -->
  <div class="samping">
    @foreach($komponen as $item)
    @foreach($item->sub_komponen as $subkom)
    @foreach($subkom->nota->sortBy('tanggal') as $nota)
    <center>
      <h4>BUKTI KAS PENGELUARAN (BKP)</h4>
    </center>
    <br>
    @php
    $total_pajak = 0;
    $jum_harga = 0;
    $r_pph21=0;
    $r_pph22=0;
    $r_ppn=0;
    @endphp

    @foreach($nota->nota_barang as $hem)
    @php
    $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
    @endphp
    @endforeach

    <!-- IF KODE PAJAK -->
    @if($nota->kode_pajak=="T")
    <!-- IF KODE PPH 22-->
    @if($nota->kode_pph=="22")
    @if($jum_harga>=1000000)
    @php
    $dasar_pajak = (100/110)*$jum_harga;
    $ppn = (10/100)*$dasar_pajak;
    $r_ppn = round($ppn, -2);
    $pph22 = (1.5/100)*$dasar_pajak;
    $r_pph22 = round($pph22, -2);
    $total_pajak = $r_ppn+$r_pph22;
    @endphp
    @endif
    <!-- END KODE PPH 22 -->
    <!-- IF KODE PPH 21 -->
    @elseif($nota->kode_pph=="21")
    @if($jum_harga>=1000000)
    @php
    $dasar_pajak = (100/110)*$jum_harga;
    $pph21 = (5/100)*$dasar_pajak;
    $r_pph21 = round($pph21, -2);
    $total_pajak = $r_pph21;
    @endphp
    @endif
    @else
    @endif
    <!-- END IF KODE PPH21 -->
    @else
    @endif
    <!-- END IF KODE PAJAK -->
    <div class="row" style="margin-bottom: 20px;">
      <div class="col-2">
        Telah terima dari<br>
        Banyaknya Uang<br>
        Untuk Pembayaran
      </div>
      <div class="col-7">
        @php
        $text_uang = terbilang($jum_harga);
        @endphp
        <i>
          Bendahara BOS SMK Pelita Gedongtataan<br>
          {{ucwords($text_uang)}}<br>
          @if($nota->uraian=="")
          {{$nota->sub_komponen->sub_komponen}}
          @else
          {{$nota->uraian}}
          @endif
        </i>
      </div>
      <div class="col-3">
        <table border="1">
          <tr>
            <td style="padding: 3px;">Nomor : {{$no}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
      <div class="col-1"></div>
      <div class="col-11">
        <table>
          <tr>
            <td style="padding: 3px;" width="200px">Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
      <div class="col-3" align="center" style="text-align:center;">
        Mengetahui / Menyetujui,<br>
        Pengguna Anggaran<br><br><br>
      </div>
      <div class="col-3" align="center" style="text-align:center;">
        <br>Ketua Pelaksana
      </div>
      <div class="col-3" align="center" style="text-align:center;">
        <br>Bendahara
      </div>
      <div class="col-3" align="center" style="text-align:center;">
        @foreach($profil as $prof)
        @if($prof->profil_key==="Desa")
        {{$prof->profil_value}},
        @endif
        @endforeach
        {{ tanggal_local($tgl_dana->tanggal) }}<br>
        Yang Menerima
      </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
      <div class="col-3" align="center" style="text-align:center;">
        <u><b>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Kepala Sekolah")
            {{$prof->profil_value}}
            @endif
            @endforeach
          </b></u>
      </div>
      <div class="col-3" align="center" style="text-align:center;">
        <u><b>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Ketua Pelaksana")
            {{$prof->profil_value}}
            @endif
            @endforeach
          </b></u>
      </div>
      <div class="col-3" align="center" style="text-align:center;">
        <u><b>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Bendahara BOS")
            {{$prof->profil_value}}
            @endif
            @endforeach
          </b></u>
      </div>
      <div class="col-3" align="center" style="text-align:center;">
        ..............................
      </div>
    </div>

    <table class="tablee">
      <thead>
        <tr>
          <th rowspan="7" width="150px" style="text-align:left;vertical-align:top;">Barang Tersebut Diterima Dengan Baik Oleh</th>
          <th colspan="2" rowspan="2" width="300px" style="text-align:center;vertical-align:middle;">Pajak yang Dipungut</th>
          <th colspan="3" style="text-align:center;vertical-align:center;">Pembebanan Terhadap Belanja</th>
        </tr>
        <tr>
          <th width="100px" style="text-align:center;vertical-align:center;">No Bukti</th>
          <th style="text-align:center;vertical-align:center;">Uraian</th>
          <th style="text-align:center;vertical-align:center;">Jumlah</th>
        </tr>
        <tr>
          <td width="80px">PPn</td>
          <td>
            @if($r_ppn==0)
            @else
            Rp. {{ number_format($r_ppn, 0, ".", ".")}}
            @endif
          </td>
          <td align="center">{{$no}}</td>
          <td rowspan="3">
            @if($nota->uraian=="")
            {{$nota->sub_komponen->sub_komponen}}
            @else
            {{$nota->uraian}}
            @endif
          </td>
          <td>Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
        </tr>
        <tr>
          <td>PPh.22</td>
          <td>
            @if($r_pph22==0)
            @else
            Rp. {{ number_format($r_pph22, 0, ".", ".")}}
            @endif
          </td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>PPh.21</td>
          <td>
            @if($r_pph21==0)
            @else
            Rp. {{ number_format($r_pph21, 0, ".", ".")}}
            @endif
          </td>
          <td></td>
          <td></td>
        </tr>
        <tr height="20px">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr style="font-weight:bold;">
          <td>Jumlah</td>
          <td>
            @if($r_ppn==0)
            @else
            Rp. {{ number_format($total_pajak, 0, ".", ".")}}
            @endif
          </td>
          <td colspan="2" align="center">Jumlah</td>
          <td>Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
    @php $no++; @endphp
    <div style='page-break-after:always'></div>
    @endforeach
    @endforeach
    @endforeach
  </div>

</body>

</html>