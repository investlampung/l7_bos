<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>Alokasi Pengelolaan Dana</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: 'Source Sans Pro', sans-serif;
    }

    .tablee,
    td,
    th {
      border: 1px solid black;
    }

    .tablee {
      border-collapse: collapse;
      width: 100%;
      margin-bottom: 10px;
    }

    th {
      height: 20px;
      text-align: center;
      font-weight: bold;
    }

    td {
      padding-left: 5px;
      padding-right: 5px;
    }
  </style>
</head>

<body>
  <!-- onload="window.print()" -->
  <div class="container">
    <center>
      <h4 class="box-title">Alokasi Pengelola Dana {{Cookie::get('tahap')}} Th. {{Cookie::get('tahun')}}</h4>
    </center>
    <br>
    @php
    $total_pengeluaran = 0;
    $total_persen = 0;
    @endphp

    @foreach($dana as $item)
    @if($item->tipe=="Masuk")
    Dana Masuk<br>
    {{ $item->tanggal->format('d-m-Y') }} - Rp. {{ number_format($item->dana, 0, ".", ".") }}<br>
    @endif
    @endforeach

    Pengambilan<br>

    @foreach($dana as $item)
    @if($item->tipe=="Tarik")
    {{ $item->tanggal->format('d-m-Y') }} - Rp. {{ number_format($item->dana, 0, ".", ".") }}<br>
    @endif
    @endforeach
    <br>

    <table class="tablee">
      <thead>
        <tr>
          <th width="30px" align="center">No</th>
          <th>Kode Rekening</th>
          <th>Komponen</th>
          <th></th>
          <th>Nilai</th>
          <th width="40px">%</th>
        </tr>
      </thead>
      <tbody>
        @foreach($komponen as $item)
        <tr>
          <td style="background-color:#ffbc00" align="center">{{ $no++ }}</td>
          <td style="background-color:#ffbc00"></td>
          <td style="background-color:#ffbc00">{{ $item->komponen }}</td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        @php $total_komponen=0; @endphp
        @foreach($item->sub_komponen as $data)
        <tr>
          <td align="center"></td>
          <td></td>
          <td>
            {{ $data->kode_1 }}.
            @if(!empty($data->kode_2))
            {{ $data->kode_2 }}.
            @endif
            @if(!empty($data->kode_3))
            {{ $data->kode_3 }}.
            @endif
            {{ $data->sub_komponen }}
          </td>
          <td align="right">
            @php $total = 0; @endphp
            @foreach($data->nota as $nota)
            @php $jum_harga = 0; @endphp
            @foreach($nota->nota_barang as $hem)
            @php
            $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
            @endphp
            @endforeach

            @if($nota->kode_pajak=="T")
            @if($nota->kode_pph=="22")
            @if($jum_harga>=1000000)
            @php
            $dasar_pajak = (100/110)*$jum_harga;
            $ppn = (10/100)*$dasar_pajak;
            $r_ppn = round($ppn, -2);
            $pph22 = (1.5/100)*$dasar_pajak;
            $r_pph22 = round($pph22, -2);
            $t_pajak = $r_ppn+$r_pph22;
            $t_harga = $jum_harga+$t_pajak;
            $total = $total+$t_harga;
            @endphp
            @endif
            @elseif($nota->kode_pph=="21")
            @if($jum_harga>=1000000)
            @php
            $dasar_pajak = (100/110)*$jum_harga;
            $pph21 = (5/100)*$dasar_pajak;
            $r_pph21 = round($pph21, -2);
            $t_harga = $jum_harga+$r_pph21;
            $total = $total+$t_harga;
            @endphp
            @endif
            @else

            @endif
            @else
            @php $total = $total+$jum_harga; @endphp
            @endif
            @endforeach
            @php $total_komponen = $total_komponen+$total; @endphp
            @if(!empty($total))
            {{ number_format($total, 0, ".", ".")}}
            @endif
          </td>
          <td></td>
          <td></td>
        </tr>
        @endforeach
        <tr>
          <td align="center"></td>
          <td></td>
          <td style="background-color:#8cde92" align="right"><b>Sub Jumlah Pengeluaran</b></td>
          <td></td>
          <td style="background-color:#8cde92">
            @if(!empty($total_komponen))
            {{ number_format($total_komponen, 0, ".", ".")}}
            @endif
          </td>
          <td style="background-color:#8cde92">
            @php
            $persen = ($total_komponen/$total_dana)*100;
            @endphp
            @if(!empty($persen))
            {{round($persen)}} %
            @endif
          </td>
        </tr>
        <tr height="20px">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        @php
        $total_pengeluaran = $total_pengeluaran+$total_komponen;
        $total_persen = $total_persen+$persen;
        @endphp
        @endforeach
        <tr>
          <td></td>
          <td></td>
          <td style="background-color:#f1885f" align="right"><b> PENGELUARAN</b></td>
          <td></td>
          <td style="background-color:#f1885f"><b>{{ number_format($total_pengeluaran, 0, ".", ".")}}</b></td>
          <td style="background-color:#f1885f">
            <b>
              @if(!empty($total_persen))
              {{round($total_persen)}} %
              @endif
            </b>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

</body>

</html>