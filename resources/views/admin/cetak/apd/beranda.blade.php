@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Alokasi Pengelolaan Dana
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-primary" target="_blank" href="{{ route('apd.show',1) }}"><i class="fa fa-print"></i> Cetak APD</a>
      <br><br>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Alokasi Pengelola Dana {{Cookie::get('tahap')}} Th. {{Cookie::get('tahun')}}</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table>
            @php
            $total_pengeluaran = 0;
            $total_persen = 0;
            @endphp

            @foreach($dana as $item)
            @if($item->tipe=="Masuk")
            <tr>
              <td width="100px">Dana Masuk</td>
              <td width="100px">: {{ $item->tanggal->format('d-m-Y') }}</td>
              <td>Rp. {{ number_format($item->dana, 0, ".", ".") }}</td>
            </tr>
            @endif
            @endforeach
            <tr>
              <td colspan="3">Pengambilan</td>
            </tr>
            @foreach($dana as $item)
            @if($item->tipe=="Tarik")
            <tr>
              <td></td>
              <td>: {{ $item->tanggal->format('d-m-Y') }}</td>
              <td>Rp. {{ number_format($item->dana, 0, ".", ".") }}</td>
            </tr>
            @endif
            @endforeach
          </table>
          <br><br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="30px" align="center">No</th>
                <th>Kode Rekening</th>
                <th>Komponen</th>
                <th></th>
                <th>Nilai</th>
                <th>%</th>
              </tr>
            </thead>
            <tbody>
              @foreach($komponen as $item)
              <tr>
                <td style="background-color:#ffbc00" align="center">{{ $no++ }}</td>
                <td style="background-color:#ffbc00"></td>
                <td style="background-color:#ffbc00">{{ $item->komponen }}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @php $total_komponen=0; @endphp
              @foreach($item->sub_komponen->sortBy('kode_1') as $data)
              <tr>
                <td align="center"></td>
                <td></td>
                <td>
                  {{ $data->kode_1 }}.
                  @if(!empty($data->kode_2))
                  {{ $data->kode_2 }}.
                  @endif
                  @if(!empty($data->kode_3))
                  {{ $data->kode_3 }}.
                  @endif
                  {{ $data->sub_komponen }}
                </td>
                <td align="right">
                  @php $total = 0; @endphp
                  @foreach($data->nota as $nota)
                  @php $jum_harga = 0; @endphp
                  @foreach($nota->nota_barang as $hem)
                  @php
                  $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
                  @endphp
                  @endforeach

                  @if($nota->kode_pajak=="T")
                  @if($nota->kode_pph=="22")
                  @if($jum_harga>=1000000)
                  @php
                  $dasar_pajak = (100/110)*$jum_harga;
                  $ppn = (10/100)*$dasar_pajak;
                  $r_ppn = round($ppn, -2);
                  $pph22 = (1.5/100)*$dasar_pajak;
                  $r_pph22 = round($pph22, -2);
                  $t_pajak = $r_ppn+$r_pph22;
                  $t_harga = $jum_harga+$t_pajak;
                  $total = $total+$t_harga;
                  @endphp
                  @endif
                  @elseif($nota->kode_pph=="21")
                  @if($jum_harga>=1000000)
                  @php
                  $dasar_pajak = (100/110)*$jum_harga;
                  $pph21 = (5/100)*$dasar_pajak;
                  $r_pph21 = round($pph21, -2);
                  $t_harga = $jum_harga+$r_pph21;
                  $total = $total+$t_harga;
                  @endphp
                  @endif
                  @else

                  @endif
                  @else
                  @php $total = $total+$jum_harga; @endphp
                  @endif
                  @endforeach
                  @php $total_komponen = $total_komponen+$total; @endphp
                  @if(!empty($total))
                  {{ number_format($total, 0, ".", ".")}}
                  @endif
                </td>
                <td></td>
                <td></td>
              </tr>
              @endforeach
              <tr>
                <td align="center"></td>
                <td></td>
                <td style="background-color:#8cde92" align="right"><b>Sub Jumlah Pengeluaran</b></td>
                <td></td>
                <td style="background-color:#8cde92">
                  @if(!empty($total_komponen))
                  {{ number_format($total_komponen, 0, ".", ".")}}
                  @endif
                </td>
                <td style="background-color:#8cde92">
                  @php
                  $persen = ($total_komponen/$total_dana)*100;
                  @endphp
                  @if(!empty($persen))
                  {{round($persen)}} %
                  @endif
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @php
              $total_pengeluaran = $total_pengeluaran+$total_komponen;
              $total_persen = $total_persen+$persen;
              @endphp
              @endforeach
              <tr>
                <td></td>
                <td></td>
                <td style="background-color:#f1885f" align="right">TOTAL PENGELUARAN</td>
                <td></td>
                <td style="background-color:#f1885f">{{ number_format($total_pengeluaran, 0, ".", ".")}}</td>
                <td style="background-color:#f1885f">
                  @if(!empty($total_persen))
                  {{round($total_persen)}} %
                  @endif
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection