@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Nota
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Cetak Nota</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Komponen</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($komponen as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->kode_komponen }}</td>
                <td>{{ $item->komponen }}</td>
                <td align="center">
                  <a class="btn btn-primary" target="_blank" href="{{ route('notas.show',$item->id) }}"><i class="fa fa-print"></i> Cetak</a>
                  <a class="btn btn-success" target="_blank" href="#"><i class="fa fa-download"></i> Download</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection