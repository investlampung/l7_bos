<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>Nota - {{$komponen->kode_komponen}}. {{$komponen->komponen}}</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    .tablee,
    td,
    th {
      border: 1px solid black;
    }

    .tablee {
      border-collapse: collapse;
      width: 100%;
      margin-bottom: 10px;
    }

    th {
      height: 20px;
      text-align: center;
    }

    td {
      padding-left: 5px;
      padding-right: 5px;
    }
  </style>
</head>

<body>
  <!-- onload="window.print()" -->
  <div class="container">
    <h4>{{$komponen->kode_komponen}}. {{$komponen->komponen}}</h4>

    @foreach($komponen->sub_komponen as $subkom)

    @if (count($subkom->nota) > 0)
    <h5>
      {{$subkom->kode_1}}.
      @if(!empty($subkom->kode_2))
      {{$subkom->kode_2}}.
      @endif
      @if(!empty($subkom->kode_3))
      {{$subkom->kode_3}}.
      @endif
      {{$subkom->sub_komponen}}
    </h5>
    @endif

    @foreach($subkom->nota as $nota)
    <table width="100%" class="tablee">
      <tbody>
        <tr>
          <th rowspan="2" width="40px">No</th>
          <th rowspan="2">Nama Barang</th>
          <th colspan="2">Banyak</th>
          <th rowspan="2" width="150px">Harga</th>
          <th rowspan="2" width="150px">Jumlah</th>
          <th rowspan="2" width="150px">Rekening</th>
        </tr>
        <tr>
          <th width="40px">Qty</th>
          <th width="100px">Satuan</th>
        </tr>

        @php $no=1; @endphp
        @php $jum_harga =0; @endphp

        @php
          $rek=0;
        @endphp
        @foreach($nota->nota_barang as $data)
        <tr>
          <td align="center">{{$no++}}</td>
          <td>{{$data->barang->barang}}</td>
          <td align="center">{{$data->qty}}</td>
          <td align="center">{{$data->barang->satuan}}</td>
          <td align="right">{{ number_format($data->barang->harga, 0, ".", ".")}}</td>
          <td align="right">
            {{number_format($data->barang->harga*$data->qty, 0, ".", ".") }}
          </td>
          <td align="center">
            @php $rek++ @endphp
            @if($rek==1)
            {{$data->nota->kode_rek}}
            @endif
          </td>
          @php
          $jum_harga = $jum_harga + ($data->barang->harga*$data->qty);
          @endphp
        </tr>
        @endforeach

        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#ffbc00">
            {{ number_format($jum_harga, 0, ".", ".") }}
          </td>
          <td></td>
        </tr>
        @if($nota->kode_pajak=="T")
        @if($nota->kode_pph=="22")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $ppn = (10/100)*$dasar_pajak;
        $r_ppn = round($ppn, -2);
        $pph22 = (1.5/100)*$dasar_pajak;
        $r_pph22 = round($pph22, -2);
        $t_pajak = $r_ppn+$r_pph22;
        $t_harga = $jum_harga+$t_pajak;
        @endphp
        @endif
        <tr>
          <td></td>
          <td align="right">PPn</td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#8cde92">{{ number_format($r_ppn, 0, ".", ".") }}</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td align="right">PPh.22</td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#8cde92">{{ number_format($r_pph22, 0, ".", ".") }}</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>Jumlah Pajak</td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#8cde92">{{ number_format($t_pajak, 0, ".", ".") }}</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><b>Total</b></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#f1885f"><b>{{ number_format($t_harga, 0, ".", ".") }}</b></td>
          <td></td>
        </tr>
        @elseif($item->kode_pph=="21")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $pph21 = (5/100)*$dasar_pajak;
        $r_pph21 = round($pph21, -2);
        $t_harga = $jum_harga+$r_pph21;
        @endphp
        @endif
        <tr>
          <td></td>
          <td align="right">PPh.21</td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#8cde92">{{ number_format($r_pph21, 0, ".", ".") }}</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>Jumlah Pajak</td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#8cde92">{{ number_format($r_pph21, 0, ".", ".") }}</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><b>Total</b></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#f1885f"><b>{{ number_format($t_harga, 0, ".", ".") }}</b></td>
          <td></td>
        </tr>
        @else

        @endif
        @else
        <tr>
          <td></td>
          <td><b>Total</b></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right" style="background-color:#f1885f"><b>{{ number_format($jum_harga, 0, ".", ".") }}</b></td>
          <td></td>
        </tr>
        @endif
      </tbody>
    </table>
    @endforeach
    @endforeach
  </div>

</body>

</html>