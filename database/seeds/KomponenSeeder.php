<?php

use Illuminate\Database\Seeder;

class KomponenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "A",
            'komponen' => 'PENGEMBANGAN PERPUSTAKAAN',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "B",
            'komponen' => 'PENERIMAAN PESERTA DIDIK BARU (PPDB)',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "C",
            'komponen' => 'KEGIATAN PEMBELAJARAN INTRA DAN EKTRA KURIKULER',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "D",
            'komponen' => 'KEGIATAN EVALUASI PEMBELAJARAN',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "E",
            'komponen' => 'ADMINISTRASI KEGIATAN SEKOLAH',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "F",
            'komponen' => 'PENG PROFESI GURU DAN TENAGA KEPENDIDIKAN SERTA PENGEMBANGAN MANAJEMEN SEKOLAH',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "G",
            'komponen' => 'LANGGANAN DAYA DAN JASA',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "H",
            'komponen' => 'PEMELIHAARAAN SARANA PRASARANA SEKOLAH',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "I",
            'komponen' => 'PENYEDIAAN ALAT MULTI MEDIA PEMBELAJARAN',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "J",
            'komponen' => 'BIAYA BURSA KERJA KHUSUS/PRAKERIN / LSP-1',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "K",
            'komponen' => 'PENY UJI KOMPETENSI DAN SERTIFIKASI KEJURUAN',
        ]);
        App\Komponen::create([
            'tahap_id' => 1,
            'kode_komponen' => "L",
            'komponen' => 'PEMBAYARAN HONOR',
        ]);
    }
}
