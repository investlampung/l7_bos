<?php

use Illuminate\Database\Seeder;

class NotaBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\NotaBarang::create([
            'nota_id' => 1,
            'barang_id' => 1,
            'qty' => 24,
        ]);
        App\NotaBarang::create([
            'nota_id' => 1,
            'barang_id' => 2,
            'qty' => 24,
        ]);

        App\NotaBarang::create([
            'nota_id' => 2,
            'barang_id' => 3,
            'qty' => 3,
        ]);
        App\NotaBarang::create([
            'nota_id' => 2,
            'barang_id' => 4,
            'qty' => 3,
        ]);
        App\NotaBarang::create([
            'nota_id' => 2,
            'barang_id' => 5,
            'qty' => 5,
        ]);
        App\NotaBarang::create([
            'nota_id' => 2,
            'barang_id' => 6,
            'qty' => 2,
        ]);
        App\NotaBarang::create([
            'nota_id' => 2,
            'barang_id' => 7,
            'qty' => 2,
        ]);
    }
}
