<?php

use Illuminate\Database\Seeder;

class ProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Profil::create([
            'profil_key' => 'NPSN',
            'profil_value' => '10815146'
        ]);
        App\Profil::create([
            'profil_key' => 'Nama',
            'profil_value' => 'SMK Pelita Gedongtataan'
        ]);
        App\Profil::create([
            'profil_key' => 'Telp',
            'profil_value' => '0721-94092'
        ]);
        App\Profil::create([
            'profil_key' => 'Email',
            'profil_value' => 'smkpelitasmk@yahoo.com'
        ]);
        App\Profil::create([
            'profil_key' => 'Alamat',
            'profil_value' => 'Jalan Raya Penengahan No. 04'
        ]);
        App\Profil::create([
            'profil_key' => 'Provinsi',
            'profil_value' => 'Lampung'
        ]);
        App\Profil::create([
            'profil_key' => 'Kabupaten',
            'profil_value' => 'Pesawaran'
        ]);
        App\Profil::create([
            'profil_key' => 'Desa',
            'profil_value' => 'Gedongtataan'
        ]);
        App\Profil::create([
            'profil_key' => 'Kepala Sekolah',
            'profil_value' => 'AUNURROFIQ M., S.Sos., M.M.'
        ]);
        App\Profil::create([
            'profil_key' => 'Komite Sekolah',
            'profil_value' => 'Dra. INDRAWATI'
        ]);
        App\Profil::create([
            'profil_key' => 'Bendahara Sekolah',
            'profil_value' => 'APRIDAYANTI., A.Mk.'
        ]);
        App\Profil::create([
            'profil_key' => 'Bendahara BOS',
            'profil_value' => 'THOHIR ZAIN'
        ]);
        App\Profil::create([
            'profil_key' => 'Ketua Pelaksana',
            'profil_value' => 'AZWAR AMIN'
        ]);
    }
}
