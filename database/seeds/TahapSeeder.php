<?php

use Illuminate\Database\Seeder;

class TahapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Tahap::create([
            'tahun_id' => 2,
            'tahap' => 'Tahap 1'
        ]);
        App\Tahap::create([
            'tahun_id' => 2,
            'tahap' => 'Tahap 2'
        ]);
        App\Tahap::create([
            'tahun_id' => 2,
            'tahap' => 'Tahap 3'
        ]);
        App\Tahap::create([
            'tahun_id' => 2,
            'tahap' => 'Tahap 4'
        ]);
    }
}
