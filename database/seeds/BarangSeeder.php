<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Barang::create([
            'toko_id' => 1,
            'tahap_id' => 1,
            'barang' => 'Mouse Logitech',
            'satuan' => 'Pcs',
            'harga' => 52000,
        ]);
        App\Barang::create([
            'toko_id' => 1,
            'tahap_id' => 1,
            'barang' => 'Keyboard Logitech MK100',
            'satuan' => 'Pcs',
            'harga' => 115000,
        ]);
        App\Barang::create([
            'toko_id' => 2,
            'tahap_id' => 1,
            'barang' => 'Baterai 9 Volt',
            'satuan' => 'Box',
            'harga' => 65000,
        ]);
        App\Barang::create([
            'toko_id' => 2,
            'tahap_id' => 1,
            'barang' => 'Timah Solder',
            'satuan' => 'Glg',
            'harga' => 60000,
        ]);
        App\Barang::create([
            'toko_id' => 2,
            'tahap_id' => 1,
            'barang' => 'Catride IP 1600 C Black',
            'satuan' => 'Pcs',
            'harga' => 195000,
        ]);
        App\Barang::create([
            'toko_id' => 2,
            'tahap_id' => 1,
            'barang' => 'Catride IP 1600 Colour',
            'satuan' => 'Pcs',
            'harga' => 225000,
        ]);
        App\Barang::create([
            'toko_id' => 2,
            'tahap_id' => 1,
            'barang' => 'RJ45',
            'satuan' => 'Bks',
            'harga' => 42000,
        ]);
    }
}
