<?php

use Illuminate\Database\Seeder;

class DanaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Dana::create([
            'tahap_id' => 1,
            'tanggal' => '2020-04-15',
            'tanggal_bku' => '2020-04-15',
            'dana' => 421920000,
            'tipe' => 'Masuk',
            'ket' => 'Penarikan Dana 1',
        ]);
        App\Dana::create([
            'tahap_id' => 1,
            'tanggal' => '2020-04-21',
            'tanggal_bku' => '2019-10-10',
            'dana' => 200000000,
            'tipe' => 'Tarik',
            'ket' => 'Penarikan Dana 1',
        ]);

        App\Dana::create([
            'tahap_id' => 1,
            'tanggal' => '2020-04-30',
            'tanggal_bku' => '2020-02-15',
            'dana' => 221920000,
            'tipe' => 'Tarik',
            'ket' => 'Penarikan Dana 2',
        ]);
    }
}
