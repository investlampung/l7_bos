<?php

use Illuminate\Database\Seeder;

class MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        App\Meta::create([
            'tahap_id' => 1,
            'meta_key' => 'Jenis Kegiatan',
            'meta_value' => 'Bantuan Operasional Sekolah'
        ]);
        App\Meta::create([
            'tahap_id' => 1,
            'meta_key' => 'Pemberi Bantuan',
            'meta_value' => 'Direktorat Pembinaan SMK DIKNAS Pusat'
        ]);
        App\Meta::create([
            'tahap_id' => 1,
            'meta_key' => 'Bulan',
            'meta_value' => 'Januari - Maret'
        ]);
        App\Meta::create([
            'tahap_id' => 1,
            'meta_key' => 'Revisi RAB',
            'meta_value' => ''
        ]);

        App\Meta::create([
            'tahap_id' => 2,
            'meta_key' => 'Jenis Kegiatan',
            'meta_value' => 'Bantuan Operasional Sekolah'
        ]);
        App\Meta::create([
            'tahap_id' => 2,
            'meta_key' => 'Pemberi Bantuan',
            'meta_value' => 'Direktorat Pembinaan SMK DIKNAS Pusat'
        ]);
        App\Meta::create([
            'tahap_id' => 2,
            'meta_key' => 'Bulan',
            'meta_value' => 'Januari - Maret'
        ]);
        App\Meta::create([
            'tahap_id' => 2,
            'meta_key' => 'Revisi RAB',
            'meta_value' => ''
        ]);


        App\Meta::create([
            'tahap_id' => 3,
            'meta_key' => 'Jenis Kegiatan',
            'meta_value' => 'Bantuan Operasional Sekolah'
        ]);
        App\Meta::create([
            'tahap_id' => 3,
            'meta_key' => 'Pemberi Bantuan',
            'meta_value' => 'Direktorat Pembinaan SMK DIKNAS Pusat'
        ]);
        App\Meta::create([
            'tahap_id' => 3,
            'meta_key' => 'Bulan',
            'meta_value' => 'Januari - Maret'
        ]);
        App\Meta::create([
            'tahap_id' => 3,
            'meta_key' => 'Revisi RAB',
            'meta_value' => ''
        ]);


        App\Meta::create([
            'tahap_id' => 4,
            'meta_key' => 'Jenis Kegiatan',
            'meta_value' => 'Bantuan Operasional Sekolah'
        ]);
        App\Meta::create([
            'tahap_id' => 4,
            'meta_key' => 'Pemberi Bantuan',
            'meta_value' => 'Direktorat Pembinaan SMK DIKNAS Pusat'
        ]);
        App\Meta::create([
            'tahap_id' => 4,
            'meta_key' => 'Bulan',
            'meta_value' => 'Januari - Maret'
        ]);
        App\Meta::create([
            'tahap_id' => 4,
            'meta_key' => 'Revisi RAB',
            'meta_value' => ''
        ]);
    }
}
