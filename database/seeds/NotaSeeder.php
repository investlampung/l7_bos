<?php

use Illuminate\Database\Seeder;

class NotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Nota::create([
            'sub_komponen_id' => 2,
            'toko_id' => 1,
            'uraian' => "Pembelian Buku",
            'kode_rek' => '5-2-2-01-01',
            'tanggal' => '2020-02-24',
            'tanggal_bku' => '2020-04-24',
            'kode_pajak' => 'T',
            'kode_pph' => '22',
        ]);
        App\Nota::create([
            'sub_komponen_id' => 2,
            'toko_id' => 2,
            'uraian' => "",
            'kode_rek' => '5-2-2-01-01',
            'tanggal' => '2020-02-26',
            'tanggal_bku' => '2020-04-24',
            'kode_pajak' => 'F',
            'kode_pph' => '21',
        ]);
    }
}
