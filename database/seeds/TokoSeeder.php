<?php

use Illuminate\Database\Seeder;

class TokoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Toko::create([
            'toko' => "Dunia",
            'jenis' => "Komputer"
        ]);
        App\Toko::create([
            'toko' => "Sentra",
            'jenis' => "Komputer"
        ]);
        App\Toko::create([
            'toko' => "Hanif",
            'jenis' => "Sport"
        ]);
    }
}
