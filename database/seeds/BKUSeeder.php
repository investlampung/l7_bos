<?php

use Illuminate\Database\Seeder;

class BKUSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\BKU::create([
            'nota_id' => 1,
            'pajak' => 0,
            'pph' =>0
        ]);
        App\BKU::create([
            'nota_id' => 2,
            'pajak' => 0,
            'pph' =>0
        ]);
    }
}
