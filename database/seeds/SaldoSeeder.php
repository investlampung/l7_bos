<?php

use Illuminate\Database\Seeder;

class SaldoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Saldo::create([
            'tahap_id' => 1,
            'tanggal' => '2020-01-01',
            'saldo' => 0,
            'ket' => 'Saldo Kas Bulan Januari',
        ]);
        App\Saldo::create([
            'tahap_id' => 1,
            'tanggal' => '2020-02-01',
            'saldo' => 0,
            'ket' => 'Saldo Kas Bulan Februari',
        ]);
        App\Saldo::create([
            'tahap_id' => 1,
            'tanggal' => '2020-03-01',
            'saldo' => 0,
            'ket' => 'Saldo Kas Bulan Maret',
        ]);
        App\Saldo::create([
            'tahap_id' => 1,
            'tanggal' => '2020-04-01',
            'saldo' => 0,
            'ket' => 'Saldo Kas Bulan April',
        ]);
    }
}
