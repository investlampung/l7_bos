<?php

use Illuminate\Database\Seeder;

class SubKomponenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'a',
            'kode_2' => '',
            'kode_3' => '',
            'sub_komponen' => 'Penyediaan Buku Teks Utama',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'a',
            'kode_2' => '1',
            'kode_3' => '',
            'sub_komponen' => 'Pemb Buku Disesuaikan Kurikulum ( K - 2013 )',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'a',
            'kode_2' => '2',
            'kode_3' => '',
            'sub_komponen' => 'Pemb Buku Memenuhi Rasio 1 Buku setiap mapel',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'a',
            'kode_2' => '3',
            'kode_3' => '',
            'sub_komponen' => 'Memenuhi Kebt Buku Untuk Guru pada setiap mapel',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'a',
            'kode_2' => '4',
            'kode_3' => '',
            'sub_komponen' => 'Buku telah dinilai Kemendiknas dan Pegangan KBM',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'b',
            'kode_2' => '',
            'kode_3' => '',
            'sub_komponen' => 'Penyediaan Buku Teks Pendamping',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'c',
            'kode_2' => '',
            'kode_3' => '',
            'sub_komponen' => 'Penyediaan Buku Non Tekhnis',
        ]);
        App\SubKomponen::create([
            'komponen_id' => 1,
            'kode_1' => 'd',
            'kode_2' => '',
            'kode_3' => '',
            'sub_komponen' => 'Pembiayaan Lain Relevan Pengembangan Perpustakaan (Peng Perustakaan, Pemeliharaan,Perabot ,Ac Dll)',
        ]);
    }
}
