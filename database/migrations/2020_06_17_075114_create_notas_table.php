<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sub_komponen_id')->unsigned();
            $table->foreign('sub_komponen_id')->references('id')->on('sub_komponens')->onDelete('cascade');
            $table->string('uraian')->nullable();
            $table->string('kode_rek')->nullable();
            $table->date('tanggal');
            $table->date('tanggal_bku')->nullable();
            $table->string('kode_pajak');
            $table->string('kode_pph');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
